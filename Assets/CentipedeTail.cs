﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CentipedeTail : MonoBehaviour {

	[Serializable] class Config {
		public Animator animator;
		public GameManager gameManager;
		public float minX = 801f;
		public float maxX = 833f;
	}

	[Serializable] class State {
		public bool shouldEmerge = false;
		public bool shouldFinishEmerge = false;
		public bool shouldScheduleAttack = false;
		public bool shouldAttack = false;
		public bool shouldHit = false;
		public bool shouldSumerge = false;
		public int activeAttack = 1;
	}

	[SerializeField] Config config;
	[SerializeField] State state;

	// Use this for initialization
	void Start () {
		config.animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if (state.shouldScheduleAttack) {
			StartCoroutine(ScheduleAttack());
			state.shouldScheduleAttack = false;
			return;
		}
		if (state.shouldAttack) {
			state.shouldAttack = false;
			Attack();
			return;
		}
		if (state.shouldHit) {
			return;
		}
		if (state.shouldSumerge) {
			return;
		}
	}

	void LateUpdate () {
		Animate();
	}

	void Animate () {
		if (state.shouldEmerge) {
			config.animator.SetTrigger("startEmerge");
			state.shouldEmerge = false;
			return;
		}
		if (state.shouldFinishEmerge) {
			config.animator.SetTrigger("emerge");
			state.shouldFinishEmerge = false;
			state.shouldScheduleAttack = true;
			return;
		}
		if (state.shouldHit) {
			if (state.activeAttack == 0) {
				config.animator.SetTrigger("hitAround");
			} else {
				config.animator.SetTrigger("hitGround");
			}
			state.shouldHit = false;
			return;
		}
		if (state.shouldSumerge) {
			config.animator.SetTrigger("sumerge");
			state.shouldSumerge = false;
			return;
		}
	}

	public void Emerge(float positionX) {
		positionX = positionX < config.minX ? config.minX : positionX > config.maxX ? config.maxX : positionX;
		transform.position = new Vector2(positionX, transform.position.y);
		state.shouldEmerge = true;
	}

	public void FinishEmerge() {
		StartCoroutine(ScheduleFinishEmerge());
	}

	void Attack () {
		state.shouldHit = true;
	}

	public void FinishAttack() {
		StartCoroutine(ScheduleSumerge());
	}

	public void NotifyEndOfSumerge() {
		config.gameManager.NotifyEndOfAttack();
	}

	int GetRandomAttack() {
		// Force Hit Around if the player is next to the centipede
		if (transform.position.x == config.maxX) {
			return 0;
		}
		float probability = UnityEngine.Random.value;
		if (probability < 0.4) {
			return 0;
		} else {
			return 1;
		}
	}

	IEnumerator ScheduleFinishEmerge() {
		yield return new WaitForSeconds(1);
		state.shouldFinishEmerge = true;
	}

	IEnumerator ScheduleAttack() {
		yield return new WaitForSeconds(3);
		state.shouldAttack = true;
		state.activeAttack = GetRandomAttack();
	}

	IEnumerator ScheduleSumerge() {
		yield return new WaitForSeconds(2);
		state.shouldSumerge = true;
	}
}
