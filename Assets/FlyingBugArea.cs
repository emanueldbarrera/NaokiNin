﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingBugArea : MonoBehaviour {

	[SerializeField] FlyingBugController flyingBug;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay2D(Collider2D collider) {
		if (collider.CompareTag("Player")) {
			flyingBug.NotifyPlayerSeen(collider.transform.position);
		}
	}
}
