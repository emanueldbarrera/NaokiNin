﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainController : MonoBehaviour {

	float length;
	int direction;
	bool isReturning = false;
	float relativeHeight = -1f;

	// Use this for initialization
	void Start () {
		transform.localPosition = new Vector2(0, relativeHeight);
		length = Mathf.Abs(gameObject.GetComponent<SpriteRenderer> ().bounds.size.x);
	}
	
	// Update is called once per frame
	void Update () {
		transform.localPosition = new Vector2(transform.localPosition.x, relativeHeight);
		if (!isReturning && Mathf.Abs(transform.localPosition.x) >= length) {
			isReturning = true;
			Move (direction*-1);
		}
	}

	public void Move(int direction){
		this.direction = direction;
		gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(40f*direction, 0);
	}

	public void OnTriggerEnter2D(Collider2D collision) {
		transform.parent.SendMessage ("setIsChainning", false);
		Destroy(gameObject);
	}
}
