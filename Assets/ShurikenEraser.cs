﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShurikenEraser : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D collider) {
		if (collider.CompareTag("shuriken")) {
			Destroy (collider.gameObject);
		}
	}
}
