﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;

public class CentipedeSpit : MonoBehaviour {

	float speed = 30f;
	Vector3 direction;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += direction * Time.deltaTime;
	}

	public void SetTarget(Vector3 target) {
		direction = (target - transform.position).normalized * speed;
	}

	public void DestroyObject() {
		StartCoroutine(ScheduleDestroy());
	}

	IEnumerator ScheduleDestroy() {
		yield return new WaitForSeconds(2);
		Destroy(gameObject);
	}
}
