﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogManager : MonoBehaviour {

	[SerializeField] string dialog;
	string dialogShown;
	string dialogToShow;
	[SerializeField] TextMesh dialogTextMesh;
	[SerializeField] MeshRenderer dialogMeshRenderer;
	int frameNumerator=0;

	// Use this for initialization
	void Start () {
		dialogMeshRenderer.sortingOrder = 5;
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		// Count frames, and skip some of them
		frameNumerator++;
		frameNumerator = frameNumerator%1;
		if (frameNumerator != 0) {
			return;
		}

		if (!dialog.Equals ("")) {
			dialogTextMesh.text = "";
			dialogShown = "";
			dialogToShow = dialog;
			dialog = "";
			return;
		}

		if (dialogToShow.Length > 0) {
			dialogShown = dialogShown + dialogToShow [0];
			dialogToShow = dialogToShow.Substring (1);
			dialogTextMesh.text = dialogShown;
		}
	}

	public void DisplayText() {
		dialogTextMesh.text = dialog;
	}
}
