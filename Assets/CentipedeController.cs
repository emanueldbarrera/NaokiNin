﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CentipedeController : MonoBehaviour {

	[Serializable] class Config {
		public int maxLife = 10;
		public GameObject spitPrefab;
		public Animator animator;
		public GameManager gameManager;
		public GameObject player;
		public CentipedeTail tail;
		public Animator animatorRocks;
		public GameObject wall;
	}

	[Serializable] class State {
		public int life;
		public bool isDamaged = false;
		public bool shouldEmerge = false;
		public bool shouldAttack = false;
		public bool lastAttackWasSpit = false;
		public bool shouldSpit = false;
		public bool canReceiveDamage = true;
		public bool isDead = false;
		public bool hasWon = false;
	}

	[SerializeField] Config config;
	[SerializeField] State state;

	// Use this for initialization
	void Start () {
		config.animator = GetComponent<Animator> ();
		state.life = config.maxLife;
	}
	
	// Update is called once per frame
	void Update () {
		if (state.isDead || state.hasWon) {
			return;
		}
		if (state.shouldAttack) {
			Attack();
			state.shouldAttack = false;
		}
	}

	void LateUpdate() {
		Animate();
	}

	void Attack() {
		if (state.lastAttackWasSpit) {
			config.tail.Emerge(config.player.transform.position.x);
		} else {
			state.shouldSpit = true;
			state.canReceiveDamage = false;
		}
		state.lastAttackWasSpit = !state.lastAttackWasSpit;
	}

	public void SpitObject () {
		GameObject spitObject = Instantiate (config.spitPrefab);
		spitObject.transform.parent = transform.parent.transform;
		spitObject.transform.localPosition = new Vector2(0, 6.7f);
		Vector3 target = config.player.transform.position;
		spitObject.transform.right = spitObject.transform.position - target;
		spitObject.GetComponent<CentipedeSpit>().SetTarget(target);
	}

	void Animate() {
		if (state.isDead) {
			config.animator.SetBool("isDead", true);
			config.animatorRocks.SetTrigger("off");
			return;
		}
		if (state.hasWon) {
			config.animator.SetBool("hasWon", true);
			return;
		}
		if (state.shouldEmerge) {
			state.shouldEmerge = false;
			config.animator.SetTrigger("emerge");
			config.animatorRocks.SetTrigger("on");
		}
		if (state.shouldSpit) {
			state.shouldSpit = false;
			config.animator.SetTrigger("spit");
		}
	}

	void OnTriggerEnter2D(Collider2D collider) {
		if (!state.canReceiveDamage) {
			return;
		}
		if (collider.CompareTag("shuriken")) {
			config.animator.SetTrigger("isDamaged");
			Destroy (collider.gameObject);
			ReduceLife();
		}
		if (collider.CompareTag("slash")) {
			config.animator.SetTrigger("isDamaged");
			ReduceLife();
		}
	}

	public void Emerge() {
		state.shouldEmerge = true;
	}

	public void NotifyEndOfEmerge() {
		config.gameManager.EndOfEmerge();
		StartCoroutine(ScheduleAttack());
	}

	public void NotifyEndOfAttack() {
		StartCoroutine(ScheduleAttack());
		state.canReceiveDamage = true;
	}

	IEnumerator ScheduleAttack() {
		yield return new WaitForSeconds(3);
		state.shouldAttack = true;
	}

	void ReduceLife() {
		state.life--;
		if (state.life <= 0) {
			Die();
		}
	}

	void Die() {
		state.isDead = true;
		Destroy(config.wall);
		Destroy(config.tail);
	}

	void Win() {
		state.hasWon = true;
		Destroy(config.wall);
		Destroy(config.tail);
	}
}
