﻿using UnityEngine;

public class DamageableEnemy : MonoBehaviour {

	[SerializeField] Transform enemy;

	int health = 1;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (health < 0) {
			return;
		}
		if (health == 0) {
			Die();
		}
	}

	public void OnTriggerEnter2D(Collider2D collider){
		if (collider.gameObject.CompareTag("shuriken")) {
			health--;
		}
		if (collider.gameObject.CompareTag("slash")) {
			health--;
		}
	}

	void Die(){
		enemy.SendMessage("Die");
	}
}
