﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class GameManager : MonoBehaviour {

	[SerializeField] GameObject lifeUI;
	[SerializeField] GameObject UI;
	[SerializeField] NaokiController player;
	[SerializeField] CentipedeController centipede;
	public int life = 10;

	[SerializeField] CinemachineVirtualCamera camera1;
	[SerializeField] CinemachineVirtualCamera camera2;
	[SerializeField] CameraController mainCamera;

	[SerializeField] AudioSource audio1;
	[SerializeField] AudioSource audio2;
	[SerializeField] AudioSource audio3;

	AudioSource activeAudio;
	AudioSource nextActiveAudio;

	bool isCrossfadingMusic = false;
	float volume = 1f;

	// Use this for initialization
	void Start () {
		InitLevel1();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.Escape)) {
			Application.Quit();
		}
		if (isCrossfadingMusic) CrossfadeMusic();
	}

	void CrossfadeMusic(){
		volume = volume - 0.05f;
		activeAudio.volume = volume;
		if (volume <= 0) {
			activeAudio.Stop();
			activeAudio = nextActiveAudio;
			activeAudio.Play();
			isCrossfadingMusic = false;
			volume = 1f;
			activeAudio.volume = volume;
		}
	}

	public void InitLevel1() {
		player.transform.position = new Vector2(436.7f, 49.2f);
		// player.transform.Rotate(new Vector3(0, 180, 0));
		camera1.enabled = true;
		camera2.enabled = false;
		mainCamera.activateLimits1 = true;
		mainCamera.activateLimits2 = false;
		activeAudio = audio1;
		activeAudio.Play();
		
	}

	public void InitLevel2() {
		UI.SetActive(false);
		player.transform.position = new Vector2(790f, -38f);
		camera1.enabled = false;
		camera2.enabled = true;
		mainCamera.activateLimits1 = false;
		mainCamera.activateLimits2 = true;
	}

	void ReduceLife() {
		life--;
		lifeUI.SendMessage("SetIndexNumber", life); // Update UI animation
		if (life <= 0) {
			// Die
			player.SendMessage ("Die");
		} else {
			player.SendMessage ("ReceiveDamage");

		}
	}

	void Die() {
		life = 0;
		lifeUI.SendMessage("SetIndexNumber", life); // Update UI animation
		player.SendMessage ("Die");
	}

	public void InitBossFight() {
		// Freeze Naoki
		player.SetIsControllable(false);
		// Make centipede emerge
		centipede.Emerge();
		// Music
		nextActiveAudio = audio2;
		isCrossfadingMusic = true;
		volume = 0f;
	}

	public void EndOfEmerge() {
		// Unfreeze Naoki
		player.SetIsControllable(true);
		// Music
		nextActiveAudio = audio3;
		isCrossfadingMusic = true;
		volume = 0f;
		// UI
		UI.SetActive(true);
		UI.transform.position = new Vector2(-82.6f, 32.3f);
		UI.transform.localScale = new Vector2(4f, 4f);
	}

	public void NotifyEndOfAttack() {
		centipede.NotifyEndOfAttack();
	}
}
