﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NaokiBodyRenderHelper : MonoBehaviour {

	public void ThrowShurikenObject() {
		gameObject.GetComponentInParent<NaokiController>().ThrowShurikenObject();
	}

	public void DamageThroughSlash() {
		gameObject.GetComponentInParent<NaokiController>().DamageThroughSlash();
	}

	public void StopDamashingThroughSlash() {
		gameObject.GetComponentInParent<NaokiController>().StopDamashingThroughSlash();
	}

	public void RestartLevel() {
		SceneManager.LoadScene("level_0");
	}

}
