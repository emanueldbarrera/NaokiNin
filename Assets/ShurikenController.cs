﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShurikenController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		SpriteRenderer sr = GetComponent<SpriteRenderer>();
		StartCoroutine(LateDestroy());
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(new Vector3(0, 0, 50f));
	}

	IEnumerator LateDestroy(){
		yield return new WaitForSeconds(5);
		Destroy(gameObject);
	}
}
