﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class SceneInputController : MonoBehaviour {

	GameObject pausableScene;
	[SerializeField] string sceneName;

	// Use this for initialization
	void Start () {
		pausableScene = GameObject.FindGameObjectWithTag("pausablescene");
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Space)) {
			SceneManager.UnloadSceneAsync (SceneManager.GetSceneByName(sceneName).buildIndex);
			ExecuteEvents.Execute<IPausableScene>(pausableScene, null, (x,y)=>x.UnpauseScene());
		}
	}
}
