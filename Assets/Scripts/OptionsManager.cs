﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.U2D;

class Option {
	public SpriteRenderer spriteRenderer;
	public Sprite imageOn;
	public Sprite imageOff;

	public Option(SpriteRenderer spriteRenderer, Sprite imageOn, Sprite imageOff) {
		this.spriteRenderer = spriteRenderer;
		this.imageOn = imageOn;
		this.imageOff = imageOff;
	}
}

public class OptionsManager : MonoBehaviour {

	[SerializeField] SpriteAtlas spriteAtlas;
	[SerializeField] SpriteRenderer start;
	[SerializeField] SpriteRenderer options;
	[SerializeField] SpriteRenderer exit;

	List<Option> optionsList = new List<Option>();
	int current = 0; // From 0 to 2

	// Use this for initialization
	void Start () {
		optionsList.Add(new Option(start, spriteAtlas.GetSprite("ui_start_on"), spriteAtlas.GetSprite("ui_start")));
		optionsList.Add(new Option(options, spriteAtlas.GetSprite("ui_options_on"), spriteAtlas.GetSprite("ui_options")));
		optionsList.Add(new Option(exit, spriteAtlas.GetSprite("ui_exit_on"), spriteAtlas.GetSprite("ui_exit")));
	}
	
	// Update is called once per frame
	void Update () {
		// Arrows
		if (Input.GetKeyDown(KeyCode.DownArrow) && current < 2) {
			Debug.Log (current);
			optionsList[current].spriteRenderer.sprite = optionsList[current].imageOff;
			current = current + 1;
			optionsList[current].spriteRenderer.sprite = optionsList[current].imageOn;
		} else if (Input.GetKeyDown(KeyCode.UpArrow) && current > 0) {
			Debug.Log (current);
			optionsList[current].spriteRenderer.sprite = optionsList[current].imageOff;
			current = current - 1;
			optionsList[current].spriteRenderer.sprite = optionsList[current].imageOn;
		}
		// Enter
		if (Input.GetKeyDown (KeyCode.Return)) {
			switch (current) {
			case 0:
				SceneManager.LoadScene ("level_0");
				break;
			case 2:
				// UnityEditor.EditorApplication.isPlaying = false;
				Application.Quit();
				break;
			}
		}
	}
}
