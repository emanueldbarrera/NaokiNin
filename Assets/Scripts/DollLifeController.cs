﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DollLifeController : MonoBehaviour {

	[SerializeField] int lives = 3;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void removeLife() {
		lives--;
		if (lives <= 0){
			Destroy (this.gameObject);
		}
	}
}
