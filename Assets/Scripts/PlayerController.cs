﻿using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public Rigidbody2D rigidBody2D;

	// Dependencies
	[SerializeField] GameObject gameManager;

	// Walk
	int direction = -1;
	[SerializeField] float speed = 10f;
	[SerializeField] bool isWalking = false;
	[SerializeField] public bool onFloor = true;
	[SerializeField] LayerMask layerFloor;
	[SerializeField] bool wasLastFrameInFloor = true;

	// Jump
	[SerializeField] GameObject feet;
	[SerializeField] float jumpForce = 1000f;
	[SerializeField] bool isJumping = false;
	[SerializeField] bool isDoubleJumping = false;
	[SerializeField] bool isDescending = false;

	// Dash
	[SerializeField] float dashSpeed = 60f;
	[SerializeField] bool isDashing = false;
	[SerializeField] int framesDashingCount = 0;
	[SerializeField] int maxFramesDashingCount = 5;

	// Shuriken
	[SerializeField] GameObject shurikenPrephab;
	[SerializeField] GameObject chainPrephab;
	bool shouldThrowShurikenObject;
	int frameShurikenThrown = 0;
	[SerializeField] int framesUntilShurikenIsThrown = 12;

	// Chain
	[SerializeField] bool isChainning = false;

	//Damage
	[SerializeField] bool isReceivingDamage = false;
	[SerializeField] int damageReceivedFrame = 0;
	int maxFramesReceivingDamage = 30;

	// Dead
	bool isDead = false;
	int deadFrame = 0;
	int maxFramesDead = 30;

	[SerializeField] Animator animator;

	void Start () {
	}

	public void OnDrawGizmos(){
		Gizmos.DrawLine(
			new Vector2(feet.transform.position.x - 1.1f/2, feet.transform.position.y + 3.1f/2),
			new Vector2(feet.transform.position.x + 1.1f/2, feet.transform.position.y - 3.1f/2 - 0.5f)
		);
	}

	void FixedUpdate () {
		int currentFrame = Time.frameCount;
		onFloor = Physics2D.OverlapArea(
			new Vector2(feet.transform.position.x - 1.1f/2, feet.transform.position.y + 3.1f/2),
			new Vector2(feet.transform.position.x + 1.1f/2, feet.transform.position.y - 3.1f/2 - 0.5f),
			layerFloor
		);

		// Check death
		if (isDead) {
			if (currentFrame > deadFrame + maxFramesDead) {
				Application.LoadLevel(Application.loadedLevel);
			}
			return;
		}

		// Check damage receiving
		if (isReceivingDamage) {
			if (currentFrame > damageReceivedFrame + maxFramesReceivingDamage) {
				rigidBody2D.velocity = Vector2.zero;
				isReceivingDamage = false;
			}
			return;
		}

		// Check chainning
		if (isChainning) {
			rigidBody2D.velocity = Vector2.zero;
			return;
		}

//		if (onFloor && animator.GetCurrentAnimatorStateInfo(0).IsName("Jump")) {
//			animator.SetTrigger ("iddle");
//		}

		int newDirection = direction;
		float newSpeedX = rigidBody2D.velocity.x;
		float newSpeedY = rigidBody2D.velocity.y;

		// Dash
		if (isDashing) {
			framesDashingCount++;
			newSpeedY = 0;
			if (framesDashingCount >= maxFramesDashingCount) {
				isDashing = false;
				newSpeedX = 0;
				framesDashingCount = 0;
				isWalking = false;
			}
		} else if(Input.GetKeyDown (KeyCode.Z)) {
			newSpeedX = dashSpeed*direction;
			newSpeedY = 0;
			isDashing = true;
		}

		// Run
		if (!isDashing) {
			if (Input.GetKey (KeyCode.RightArrow)) {
				newDirection = 1;
				newSpeedX = speed;
				isWalking = true;
			} else if (Input.GetKey (KeyCode.LeftArrow)) {
				newDirection = -1;
				newSpeedX = -1 * speed;
				isWalking = true;
			}
			if (Input.GetKeyUp (KeyCode.RightArrow) || Input.GetKeyUp (KeyCode.LeftArrow)) {
				isWalking = false;
				newSpeedX = 0;
			}
		}

		rigidBody2D.velocity = new Vector2(newSpeedX, newSpeedY);

		// Rotate
		if (direction != newDirection) {
			direction = newDirection;
			rigidBody2D.transform.Rotate(new Vector3(0, direction*180, 0));
		}

		// Jump
		if (onFloor) {
			isJumping = false;
			isDoubleJumping = false;
		}
		if (!isJumping && Input.GetKey (KeyCode.Space) && !isDashing) {
			if (onFloor) {
				rigidBody2D.velocity = new Vector2 (rigidBody2D.velocity.x, 0);
				rigidBody2D.AddForce (new Vector2 (0, jumpForce));
				isJumping = true;
			} else if (isJumping && !isDoubleJumping) {
				rigidBody2D.velocity = new Vector2 (rigidBody2D.velocity.x, 0);
				rigidBody2D.AddForce (new Vector2 (0, jumpForce));
				isDoubleJumping = true;
			}
//			StartCoroutine (Jumppp ());
//			rigidBody2D.AddForce (new Vector2 (0, jumpForce));
//			animator.SetTrigger ("jump");
		}
		// If when jumping you are elevating and stop pressing SpaceBar, stop elevating
		isDescending = rigidBody2D.velocity.y <= 0;
		if (isJumping && Input.GetKeyUp (KeyCode.Space) && !isDescending) {
			rigidBody2D.velocity = new Vector2 (rigidBody2D.velocity.x, 0);
		}

		// Throw shuriken
		if (!shouldThrowShurikenObject && Input.GetKeyDown (KeyCode.X)) {
			shouldThrowShurikenObject = true;
			animator.SetTrigger("throwShuriken");
			frameShurikenThrown = currentFrame;
		}
		if (shouldThrowShurikenObject && currentFrame >= frameShurikenThrown + framesUntilShurikenIsThrown) {
			shouldThrowShurikenObject = false;
			GameObject shuriken = Instantiate(shurikenPrephab);
			shuriken.transform.position = new Vector2(transform.position.x + 1.763f * direction, transform.position.y - 0.723f);
			shuriken.GetComponent<Rigidbody2D>().velocity = new Vector2(50f*direction, 0);
		}

		// Throw chain
		if (!isChainning && Input.GetKeyDown (KeyCode.C)) {
			GameObject chain = Instantiate(chainPrephab);
			chain.transform.parent = transform;
			if (direction == -1) chain.transform.Rotate(new Vector3(0, direction*180, 0));
			chain.SendMessage ("Move", direction);
			isChainning = true;
		}

		// Animations
		// Animate(isWalking, isJumping, isDescending, isDoubleJumping, isDashing);

		wasLastFrameInFloor = onFloor;
	}

	void Animate(bool isWalking, bool isJumping, bool isDescending, bool isDoubleJumping, bool isDashing) {
		if (isDashing) {
			animator.SetTrigger("isDashing");
			return;
		}

		if (isWalking && !isJumping) {
			animator.SetBool ("isWalking", true);
		}
		if (!isWalking) {
			animator.SetBool ("isWalking", false);
		}
		if (isJumping) {
			// If when jumping you are descending, play the descending animation
			if (isDescending) {
				animator.SetBool ("isDescending", true);
			} else {
				animator.SetBool ("isDescending", false);
			}
			animator.SetBool ("isJumping", true);
		} else {
			animator.SetBool ("isDescending", false);
			if (!wasLastFrameInFloor) {
				animator.SetBool ("isJumping", false);
			}
		}

		if (isDoubleJumping) {
			animator.SetBool ("isDoubleJumping", true);
		} else {
			animator.SetBool ("isDoubleJumping", false);
		}
	}

	public IEnumerator Jumppp(){
		isJumping = true;
		yield return new WaitForSeconds (1);
		yield return null;
	}

	public void ReceiveDamage() {
		// Check if it is ok to receive damage now
		int currentFrame = Time.frameCount;
		if (currentFrame <= damageReceivedFrame + maxFramesReceivingDamage) {
			return;
		}

		// Push character
		rigidBody2D.velocity = Vector2.zero;
		rigidBody2D.AddForce(new Vector2(300f * direction * -1, 600f));


		// Animate
		animator.SetTrigger("isDamaged");

		// Do not let Player to receive damage for a while
		damageReceivedFrame = currentFrame;
		isReceivingDamage = true;
	}

	public void Die() {
		animator.SetTrigger("die");
		isDead = true;
		deadFrame = Time.frameCount;
	}

	public void OnCollisionEnter2D(Collision2D collision){
		// If it collides with an enemy, get damage
		if (collision.gameObject.CompareTag("enemy")){
			gameManager.SendMessage ("ReduceLife");
		}
		if (collision.gameObject.CompareTag("instant-death")){
			gameManager.SendMessage ("Die");
		}
	}

	public void setIsChainning(bool isChainning) {
		this.isChainning = isChainning;
	}
}
