﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameInputController : MonoBehaviour, IPausableScene {

	[SerializeField] Camera mainCamera;
	bool isSceneActive = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (isSceneActive && Input.GetKeyDown (KeyCode.I)) {
			StartCoroutine(OpenScene("abilities"));
		} else if (isSceneActive && Input.GetKeyDown (KeyCode.P)) {
			StartCoroutine(OpenScene("weapons"));
		}
	}

	public IEnumerator OpenScene(string sceneName) {
		AsyncOperation scenePromise = SceneManager.LoadSceneAsync (sceneName, LoadSceneMode.Additive);
		bool isLoading = true;
		while (isLoading) {
			if (scenePromise.isDone) {
				isLoading = false;
			}
			yield return null;
		}
		mainCamera.gameObject.SetActive (false);
		Time.timeScale = 0;
		isSceneActive = false;
	}

	public void UnpauseScene() {
		if (!isSceneActive) {
			isSceneActive = true;
			mainCamera.gameObject.SetActive (true);
			Time.timeScale = 1;
		}
	}
}
