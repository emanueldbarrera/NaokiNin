﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class MappedSpriteController : MonoBehaviour {

	[SerializeField] SpriteAtlas atlas;
	[SerializeField] SpriteRenderer spriteRenderer;
	[SerializeField] string atlasIndexName;
	[SerializeField] string atlasIndexNumber;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (atlasIndexNumber != "") {
			spriteRenderer.sprite = atlas.GetSprite (atlasIndexName + atlasIndexNumber);
		}
	}

	void SetIndexNumber(int indexNumber) {
		atlasIndexNumber = indexNumber.ToString ();
	}
}
