﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DollHitController : MonoBehaviour {

	[SerializeField] Animator animator;
	[SerializeField] string trigger;
	[SerializeField] DollLifeController lifeController;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D collision) {
		Debug.Log (collision.gameObject.tag);
		if (collision.gameObject.tag.Equals("Damager")) {
			animator.SetTrigger (trigger);
			lifeController.SendMessage ("removeLife");
		}
	}
}
