﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShackleController : MonoBehaviour {

	[SerializeField] bool hasSibling = false;

	void Start () {
		// Create shackle
		SpriteRenderer shackleSpriteRenderer = this.gameObject.AddComponent<SpriteRenderer> ();
		shackleSpriteRenderer.sprite = Resources.Load("Sprites/shackleA", typeof(Sprite)) as Sprite;

		// Resize shackle
		gameObject.transform.localScale = new Vector2(0.1f, 0.1f);

		//Move main shackle
		Rigidbody2D rb2D = gameObject.AddComponent<Rigidbody2D>();
		rb2D.gravityScale = 0;
		rb2D.velocity = new Vector2 (10f, 0f);
	}

	void Update () {
		//Generate another shackle
		if (!hasSibling) {
			float width = GetComponent<SpriteRenderer>().bounds.size.x;
			Debug.Log (width);
			if (transform.localPosition.x > width) {
				CreateShackle (width);
				hasSibling = true;
			}
		}
	}

	void CreateShackle(float width) {
		GameObject newShackle = Instantiate(Resources.Load("Prefabs/Shackle")) as GameObject;
		newShackle.transform.parent = transform.parent;
		newShackle.transform.position = new Vector2 (transform.position.x - width, transform.position.y);
	}
}
