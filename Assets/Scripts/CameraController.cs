﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class CameraController : MonoBehaviour {
	
	[Serializable] class LevelLimits {
		public float minX = 436f;
		public float minY = 51f;
		public float maxX;
		public float maxY;
	}

	[SerializeField] LevelLimits levelLimits1;
	[SerializeField] LevelLimits levelLimits2;
	
	[SerializeField] public bool activateLimits1;
	[SerializeField] public bool activateLimits2;
	

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void LateUpdate () {
		if (!activateLimits1 && !activateLimits2) {
			return;
		}
		LevelLimits levelLimits = levelLimits1;
		if (activateLimits2) levelLimits = levelLimits2;
		transform.position = new Vector3 (
			transform.position.x < levelLimits.minX ? levelLimits.minX : transform.position.x > levelLimits.maxX ? levelLimits.maxX : transform.position.x,
			transform.position.y < levelLimits.minY ? levelLimits.minY : transform.position.y > levelLimits.maxY ? levelLimits.maxY : transform.position.y,
			transform.position.z
		);
	}
}
