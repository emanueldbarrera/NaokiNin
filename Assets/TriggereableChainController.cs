﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggereableChainController : MonoBehaviour {

	[SerializeField] GameObject shacklePrefab;
	GameObject mainShackle;

	void Start () {
		// Create main shackle
		mainShackle = Instantiate(shacklePrefab);
		mainShackle.transform.parent = transform;
		mainShackle.transform.position = transform.position;
//		Rigidbody2D rb2D = mainShackle.GetComponent<Rigidbody2D> () as Rigidbody2D;
//		rb2D.velocity = new Vector2 (1f, 0f);
		mainShackle.SendMessage("IsLeader", true);
	}

	void Update () {
		
	}
}
