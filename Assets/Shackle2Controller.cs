﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shackle2Controller : MonoBehaviour {

	public GameObject olderSibling;
	bool hasYoungerSibling = false;
	bool isLeader = false;
	float width;
	float height;

	// Use this for initialization
	void Start () {
		SpriteRenderer shackleSpriteRenderer = this.gameObject.AddComponent<SpriteRenderer> ();
		shackleSpriteRenderer.sprite = Resources.Load("Sprites/shackleA", typeof(Sprite)) as Sprite;

		// Resize shackle
		gameObject.transform.localScale = new Vector2(0.1f, 0.1f);

		width = GetComponent<SpriteRenderer> ().bounds.size.x;
		height = GetComponent<SpriteRenderer> ().bounds.size.y;
		transform.position = new Vector3 (transform.position.x + 8f, transform.position.y, transform.position.z);

		// Set position of joint
//		Debug.DrawLine(
//			new Vector2(transform.position.x, transform.position.y),
//			new Vector2(width - transform.position.x, height - transform.position.y),
//			Color.green,
//			20,
//			false
//		);
		Transform child = gameObject.transform.Find("Joint");
		child.localPosition = new Vector2 (
			width - transform.position.x,
			height - transform.position.y
		);
	}

	void Update() {
		if (olderSibling != null) {
			// Re-position
			UpdatePosition ();
		}

		if (!hasYoungerSibling) {
			// Generate another shackle
			if (transform.localPosition.x > width) {
//				CreateShackle ();
				hasYoungerSibling = true;
			}
		}

		if (isLeader) {
//			transform.localPosition = transform.localPosition + (new Vector3(0.1f, 0f, 0f) * Time.deltaTime);
		}
	}

	void CreateShackle() {
		GameObject newShackle = Instantiate(Resources.Load("Prefabs/Shackle")) as GameObject;
		newShackle.transform.parent = transform.parent;
		newShackle.SendMessage ("SetSibling", gameObject);
	}

	void UpdatePosition() {
		Transform joint = olderSibling.transform.Find ("Joint");
		transform.position = joint.position;
		transform.rotation = olderSibling.transform.rotation;
	}

	void SetSibling(GameObject sibling) {
		this.olderSibling = sibling;
	}

	void IsLeader(bool isLeader) {
		this.isLeader = isLeader;
	}
}