﻿using System.Collections;
using UnityEngine;
using PathCreation;
using System;

public class FlyingBugController : MonoBehaviour {

	[Serializable] class ConfigData {
		public Animator animator;
		public PathCreator pathCreator;
		public EndOfPathInstruction endInstruction = EndOfPathInstruction.Loop;
		public float speed = 7f;
		public float attackSpeed = 14f;
	}

	[Serializable] class StateData {
		public int direction = -1;
		public float distanceTravelled;
		public float attackDistanceTravelled;
		public bool isAttacking = false;
		public bool isComingBack = false;
		public Vector3 attackTarget;
		public bool isDead = false;
	}

	[Serializable] class BookKeepingData {
		public Vector3 previousPosition;
	}

	[SerializeField] ConfigData config;
	[SerializeField] StateData state;
	[SerializeField] BookKeepingData bookKeeping;

	void Start () {
		transform.position = config.pathCreator.path.GetPoint (0);
		config.animator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (state.isDead) {
			return;
		}
		if (state.isAttacking) {
			Attack ();
			return;
		} else if (state.isComingBack) {
			ComeBack ();
			return;
		}

		MoveInPath ();
	}

	void MoveInPath() {
		float currentX = transform.position.x;
		state.distanceTravelled += config.speed * Time.deltaTime;
		transform.position = config.pathCreator.path.GetPointAtDistance (state.distanceTravelled, config.endInstruction);
		if(Mathf.Sign(transform.position.x - currentX) != Mathf.Sign(state.direction)) {
			state.direction = state.direction * -1;
			transform.Rotate(new Vector3(0, state.direction*180, 0));
		}
	}

	public void NotifyPlayerSeen(Vector3 target) {
		if(!state.isAttacking && !state.isComingBack){
			InitAttack(target);
		}
	}

	void InitAttack(Vector3 attackTarget) {
		state.isAttacking = true;
		config.animator.SetBool ("isAttacking", true);
//		this.attackTarget = attackTarget;
		bookKeeping.previousPosition = transform.position;
		state.attackTarget = attackTarget;
	}

	void Attack() {
		float step = config.attackSpeed * Time.deltaTime;
		Vector3 newPosition = Vector3.MoveTowards(transform.position, state.attackTarget, step);
		if (
			Mathf.Abs(newPosition.x - state.attackTarget.x) < 0.1f &&
			Mathf.Abs(newPosition.y - state.attackTarget.y) < 0.1f
		) {
			state.isAttacking = false;
			state.isComingBack = true;
			config.animator.SetBool ("isAttacking", false);
		} else {
			transform.position = newPosition;
		}
	}

	void ComeBack() {
		float step = config.attackSpeed * Time.deltaTime;
		Vector3 newPosition = Vector3.MoveTowards(transform.position, bookKeeping.previousPosition, step);
		if (
			Mathf.Abs(newPosition.x - bookKeeping.previousPosition.x) < 0.1f &&
			Mathf.Abs(newPosition.y - bookKeeping.previousPosition.y) < 0.1f
		) {
			StartCoroutine(ScheduleEndOfComeBack());
		} else {
			transform.position = newPosition;
		}
	}

	public void Die() {
		gameObject.tag = "Untagged";
		config.animator.SetTrigger ("die");
		state.isDead = true;
	}

	public void FinishDying() {
		Destroy(transform.parent.gameObject);
	}

	IEnumerator ScheduleEndOfComeBack() {
		yield return new WaitForSeconds(1);
		state.isComingBack = false;
	}

}
