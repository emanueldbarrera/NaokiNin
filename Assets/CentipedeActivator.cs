﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CentipedeActivator : MonoBehaviour {

	[SerializeField] GameObject centipedePrephab;
	[SerializeField] GameManager gameManager;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D collider) {
		if (collider.CompareTag ("Player")) {
			gameManager.InitBossFight();
			Destroy (this);
		}
	}
}
