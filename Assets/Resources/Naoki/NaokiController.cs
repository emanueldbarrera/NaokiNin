﻿using UnityEngine;
using System;

public class NaokiController : MonoBehaviour {

	[Serializable] class ConfigData {
		public GameObject gameManager;
		public Rigidbody2D rigidbody2D;
		public Animator animator;
		public Animator armAnimator;
		public LayerMask layerGround;
		public GameObject shurikenPrephab;
		public GameObject slashDamagePrephab;
		public float runningSpeed = 10f;
		public float jumpSpeed = 1200f;
		public float damageSpeedX = 100f;
		public float damageSpeedY = 500f;
		public float dashSpeed = 600f;
		public float shurikenSpeed = 50f;
	}

	[Serializable] class StateData {
		public int direction = 1;
		public bool isRunning = false;
		public bool onGround = false;
		public bool isJumping = false;
		public bool shouldJump = false;
		public bool isDoubleJumping = false;
		public bool shouldDoubleJump = false;
		public bool isReceivingDamage = false;
		public bool shouldReceiveDamage = false;
		public bool isDashing = false;
		public bool shouldDash = false;
		public bool isEnabledToDash = false;
		public bool isThrowingShuriken = false;
		public bool shouldThrowShuriken = false;
		public bool isSlashing = false;
		public int slashToApply = 1;
		public GameObject slashDamage;
		public bool isDead = false;
		public bool isControllable = true;
	}

	[Serializable] class BookKeepingData {
		public int previousDirection = 1;
	}

	[SerializeField] ConfigData config;
	[SerializeField] StateData state;
	[SerializeField] BookKeepingData bookKeeping;

	void Start () {
		config.rigidbody2D = GetComponent<Rigidbody2D>();
	}

	void FixedUpdate () {
		if (state.isDead || state.isReceivingDamage) {
			return;
		}
		if (state.shouldReceiveDamage) {
			config.rigidbody2D.velocity = Vector2.zero;
			config.rigidbody2D.AddForce(new Vector2(config.damageSpeedX * -1 * state.direction, config.damageSpeedY));
			state.shouldReceiveDamage = false;
			state.isReceivingDamage = true;
			state.isRunning = false;
			return;
		}

		if (state.shouldDash) {
			config.rigidbody2D.AddForce(new Vector2(config.dashSpeed * state.direction, 0));
			state.shouldDash = false;
			state.isDashing = true;
			return;
		}

		if (state.isDashing) {
			config.rigidbody2D.velocity = new Vector2(config.rigidbody2D.velocity.x, 0);
			return;
		}

		if (state.isSlashing) {
			// config.rigidbody2D.velocity = new Vector2(0, config.rigidbody2D.velocity.y);
			return;
		}

		if (state.isRunning) {
			config.rigidbody2D.velocity = new Vector2(config.runningSpeed * state.direction, config.rigidbody2D.velocity.y);
		} else {   
			config.rigidbody2D.velocity = new Vector2(0f, config.rigidbody2D.velocity.y);
		}

		if (state.shouldJump) {
			state.shouldJump = false;
			state.isJumping = true;
			config.rigidbody2D.velocity = new Vector2 (config.rigidbody2D.velocity.x, 0);
			config.rigidbody2D.AddForce (new Vector2 (0, config.jumpSpeed));
		}

		if (state.shouldDoubleJump) {
			state.shouldDoubleJump = false;
			state.isDoubleJumping = true;
			config.rigidbody2D.velocity = new Vector2 (config.rigidbody2D.velocity.x, 0);
			config.rigidbody2D.AddForce (new Vector2 (0, config.jumpSpeed));
		}
	}
	
	void Update () {
		if (!state.isControllable) {
			state.isRunning = false;
			return;
		}
		// Receiving Damage blocks everything, being dead too
		if (state.isReceivingDamage) {
			return;
		}
		if (state.isDead) {
			Destroy(config.armAnimator.gameObject);
			return;
		}
		// Dash
		if (state.isEnabledToDash && !state.isDashing && Input.GetButtonDown ("Dash")) {
			state.shouldDash = true;
			state.isJumping = false;
			state.isDoubleJumping = false;
			state.isEnabledToDash = false;
			return;
		}
		// Slash
		if (!state.isSlashing && Input.GetButtonDown ("Slash")) {
			state.isSlashing = true;
			state.isRunning = false;
			state.slashToApply = (state.slashToApply + 1) % 3 + 1;
			return;
		}
		// Throw Shuriken
		if (!state.isThrowingShuriken && Input.GetButtonDown ("Shuriken")) {
			state.isThrowingShuriken = true;
		}
		// Run
		if (Input.GetAxis("Horizontal") > 0) {
			state.direction = 1;
			state.isRunning = true;
		} else if (Input.GetAxis("Horizontal") < 0) {
			state.direction = -1;
			state.isRunning = true;
		} else {
			state.isRunning = false;
		}
		// Jump && DoubleJump
		state.onGround = IsOnGround();
		if (state.onGround) {
			state.isJumping = false;
			state.isDoubleJumping = false;
			state.isEnabledToDash = true;
		}
		if (state.onGround && !state.isJumping && Input.GetButtonDown ("Jump")) {
			state.shouldJump = true;
		}
		if (!state.onGround && state.isJumping && !state.isDoubleJumping && Input.GetButtonDown ("Jump")) {
			state.shouldDoubleJump = true;
		}
	}

	void LateUpdate() {
		Animate();
	}
 
	void Animate() {
		config.animator.SetBool("isDead", state.isDead);
		if (state.isDead) {
			return;
		}
		config.animator.SetBool("isDamaged", state.isReceivingDamage);
		if (state.isReceivingDamage) {
			return;
		}
		config.animator.SetBool("isDashing", state.isDashing);
		if (state.isDashing) {
			return;
		}
		config.animator.SetBool("isSlashing", state.isSlashing);
		if (state.isSlashing) {
			config.animator.SetInteger("slashNumber", state.slashToApply);
			return;
		}
		config.animator.SetBool("isThrowingShuriken", state.isThrowingShuriken);
		config.armAnimator.SetBool("isThrowingShuriken", state.isThrowingShuriken && state.isRunning);
		if (state.direction != bookKeeping.previousDirection) {
			bookKeeping.previousDirection = state.direction;
			transform.Rotate(new Vector3(0, state.direction*180, 0));
		}
		config.animator.SetBool("isRunning", state.isRunning && state.onGround);
		config.armAnimator.SetBool("isRunning", state.isRunning && state.onGround);
		config.animator.SetBool("isJumping", state.isJumping);
		config.animator.SetBool("isDoubleJumping", state.isDoubleJumping);
	}

	bool IsOnGround() {
		bool onGround = Physics2D.OverlapArea(
			new Vector2(transform.position.x - 0.2f - 1.2f/2f, transform.position.y - 1 - 1.1f/2f),
			new Vector2(transform.position.x - 0.2f + 1.2f/2f, transform.position.y - 1 - 1.1f - 0.1f),
			config.layerGround
		);
		return onGround;
	}

	// public void OnCollisionEnter2D(Collision2D collision){
	// 	// If it collides with an enemy, get damage
	// 	if (collision.gameObject.CompareTag("enemy")){
	// 		config.gameManager.SendMessage ("ReduceLife");
	// 	}
	// 	if (collision.gameObject.CompareTag("instant-death")){
	// 		config.gameManager.SendMessage ("Die");
	// 	}
	// }

	public void OnTriggerEnter2D(Collider2D collider) {
		if (collider.CompareTag("enemy")){
			config.gameManager.SendMessage ("ReduceLife");
		}
		if (collider.CompareTag("instant-death")){
			config.gameManager.SendMessage ("Die");
		}
	}

	public void Die() {
		state.isDead = true;
	}

	public void ReceiveDamage() {
		if (state.isReceivingDamage || state.isSlashing) {
			return;
		}
		state.shouldReceiveDamage = true;
	}

	public void StopReceivingDamage() {
		state.isReceivingDamage = false;
	}

	public void ThrowShurikenObject() {
		GameObject shuriken = Instantiate(config.shurikenPrephab);
		shuriken.transform.position = new Vector2(transform.position.x + 1.763f * state.direction, transform.position.y - 0.723f);
		shuriken.GetComponent<Rigidbody2D>().velocity = new Vector2(config.shurikenSpeed * state.direction, 0);
	}

	public void DamageThroughSlash() {
		GameObject slashDamage = Instantiate(config.slashDamagePrephab);
		slashDamage.transform.position = new Vector2(transform.position.x + 1.5f * state.direction, transform.position.y -  0.6f);
		slashDamage.transform.parent = transform;
		state.slashDamage = slashDamage;
	}

	public void StopDamashingThroughSlash() {
		Destroy(state.slashDamage);
		state.isSlashing = false;
	}

	public void StopThrowingShuriken() {
		state.isThrowingShuriken = false;
	}

	public void StopSlashing() {
		state.isSlashing = false;
	}

	public void StopDashing() {
		state.isDashing = false;
	}

	public void SetIsControllable(bool isControllable) {
		state.isControllable = isControllable;
	}

	public void OnDrawGizmos(){
		Gizmos.DrawLine(
			new Vector2(transform.position.x - 0.2f - 1.2f/2f, transform.position.y - 1 - 1.1f/2f),
			new Vector2(transform.position.x - 0.2f + 1.2f/2f, transform.position.y - 1 - 1.1f + 0.1f)
		);
	}
}
