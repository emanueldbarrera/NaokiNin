﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandDisplayer : MonoBehaviour {

	[SerializeField] GameObject display;
	[SerializeField] TextMesh commnadTextMesh;
	[SerializeField] TextMesh buttonTextMesh;
	[SerializeField] MeshRenderer buttonMeshRenderer;
	[SerializeField] MeshRenderer commandMeshRenderer;
	[SerializeField] string text;
	[SerializeField] string button;
	Animator animator;

	// Use this for initialization
	void Start () {
		commnadTextMesh.text = text;
		buttonTextMesh.text = button;
		animator = display.GetComponent<Animator> ();

		buttonMeshRenderer.sortingOrder = 3;
		commandMeshRenderer.sortingOrder = 3;
	}
	
	// Update is called once per frame
	void Update () {
		// Display ();
	}

	public void Display(){
		animator.SetTrigger ("show");
		StartCoroutine (Hide (3));
	}

	public IEnumerator Hide(int seconds){
		yield return new WaitForSeconds (seconds);
		animator.SetTrigger ("hide");
		yield return null;
	}
}
